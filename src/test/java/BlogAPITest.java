import com.isaac.PostAPI;
import com.isaac.UserAPI;
import com.isaac.dto.PostDTO;
import com.isaac.dto.UserSignInDTO;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class BlogAPITest {

    private PostDTO postDTO;
    private PostAPI postAPI;
    private UserAPI userAPI;
    private String token;


    @BeforeClass
    void setUp(){
        postDTO = new PostDTO("New Post", "This is a new post");
        postAPI = new PostAPI();
        userAPI = new UserAPI();
    }

    @Test
    void shouldUserLogin(){
        Response loginResponse = userAPI.loginUser(new UserSignInDTO("kwami@email.com", "password"));
        loginResponse.then().statusCode(200);
        token = loginResponse.jsonPath().getString("token");
    }

    @Test(dependsOnMethods = "shouldUserLogin")
    void shouldCreatePost(){
        Response response = postAPI.createPost(postDTO, token);
        response.then().statusCode(200);
    }

    @Test(dependsOnMethods = "shouldCreatePost")
    void shouldGetAllPosts(){
        Response response = postAPI.getAllPosts(token);
        response.then().statusCode(200);
    }

    @Test(dependsOnMethods = "shouldCreatePost")
    void shouldGetPostByTitle(){
        Response response = postAPI.getPostById(postDTO.title(), token);
        response.then().statusCode(200);
    }

    @Test(dependsOnMethods = "shouldCreatePost")
    void shouldUpdatePost(){
        Response response = postAPI.updatePost("1", postDTO, token);
        response.then().statusCode(200);
    }

    @Test(dependsOnMethods = "shouldCreatePost")
    void shouldDeletePost(){
        Response response = postAPI.deletePost("1", token);
        response.then().statusCode(200);
    }

}
