import com.github.tomakehurst.wiremock.WireMockServer;
import io.restassured.RestAssured;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class RestAssuredClassTest {

    WireMockServer wireMockServer = new WireMockServer(1010);

    @BeforeClass
    void setup() {
        System.out.println("Starting WireMock Server");
        wireMockServer.start();
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 1010;
        System.out.println("WireMock Server started");
    }

    @AfterClass
    void tearDown() {
        wireMockServer.stop();
        System.out.println("WireMock Server stopped");
    }

    @Test
    void test() {
        wireMockServer.stubFor(post(urlPathTemplate("/api/v1/calculator/add"))
                .withHeader("Content-Type", equalTo("application/json"))
                .withRequestBody(equalToJson("{\"firstNumber\": 10, \"secondNumber\": 5}"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"result\": 15}")
                ));

        RestAssured.given()
                .body("{\"firstNumber\": 10, \"secondNumber\": 5}")
                .header("Content-Type", "application/json")
                .when()
                .post("/api/v1/calculator/add")
                .then()
                .statusCode(200)
                .log().all();
    }
}
