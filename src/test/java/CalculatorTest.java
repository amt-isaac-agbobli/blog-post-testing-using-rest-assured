import com.isaac.dto.Request;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class CalculatorTest {
    final String baseUrl = "http://localhost:8082/api/v1/calculator";

    @Test
    void getHistoryTest() {
        // Test code goes here
       var response = given()
            .when()
               .baseUri(baseUrl)
            .get("/history")
            .then()
            .statusCode(200);

        response.log().body();
    }

    @Test
    void getHistoryByIdTest() {
        String endpoint = "http://localhost:8082/api/v1/calculator/history/1";
        var response = given()
            .when()
            .get(endpoint)
            .then()
            .statusCode(200);

        response.log().all();

    }

    @Test
    void  subtractTest() {
        String endpoint = "http://localhost:8082/api/v1/calculator/subtract";
        String body = "{\"firstNumber\": 10, \"secondNumber\": 5}";

        var response = given()
            .body(body)
            .header("Content-Type", "application/json")
            .when()
            .post(endpoint)
            .then();

        response.log().status();
    }

    @Test
    void  multiplyTest() {
        String endpoint = "http://localhost:8082/api/v1/calculator/multiply";
        String body = """
                {
                "firstNumber": 10,
                "secondNumber": 5
            }
            """;

        var response = given()
            .body(body)
            .header("Content-Type", "application/json")
            .when()
            .post(endpoint)
            .then();

        response.log().status();
    }

    @Test
    void addTest(){
        String endpoint = "http://localhost:8082/api/v1/calculator/add";

        Request body = new Request(10, 5);

        var response = given()
            .body(body)
            .header("Content-Type", "application/json")
            .when()
            .post(endpoint)
            .then();

        response.log().status();

    }

    @Test
    void  deleteHistoryTest() {

        var response = given()
            .when()
                .baseUri(baseUrl)
            .delete("/history/clear")
            .then();

        response.log().all();
    }
}
