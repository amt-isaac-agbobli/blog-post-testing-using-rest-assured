import com.isaac.dto.UserSignInDTO;
import com.isaac.UserAPI;
import com.isaac.dto.UserSignUpDTO;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class UserAPITest {

    private UserAPI userAPI;
    private UserSignInDTO userSignInDTO;
    private UserSignUpDTO userSignUpDTO;

    @BeforeClass
    public void setup() {
        userAPI = new UserAPI();
        userSignUpDTO = new UserSignUpDTO("kwami@email.com", "John Doe",  "password");
        userSignInDTO = new UserSignInDTO("kwami@email.com", "password");
    }

    @Test
    public void shouldRegisterUser() {
        Response response = userAPI.registerUser(userSignUpDTO);
        response.then().statusCode(200);
    }

    @Test
    public void shouldLoginUser() {
        Response response = userAPI.loginUser(userSignInDTO);
        response.then().statusCode(200);
    }
}