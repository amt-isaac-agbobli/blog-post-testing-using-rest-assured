package com.isaac;

import com.isaac.dto.PostDTO;
import io.restassured.http.ContentType;
import io.restassured.response.Response;


import static io.restassured.RestAssured.given;

public class PostAPI {
    private static final String BASE_URL = "http://localhost:9091/api/v1/";

    public Response createPost(PostDTO postDTO, String token) {
        return given()
                .baseUri(BASE_URL)
                .body(postDTO)
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + token)
                .when()
                .post("posts");
    }

    public Response getAllPosts(String token) {
        return given()
                .baseUri(BASE_URL)
                .header("Authorization", "Bearer " + token)
                .when()
                .get("posts/all");
    }

    public Response getPostById(String postId, String token) {
        return given()
                .baseUri(BASE_URL)
                .header("Authorization", "Bearer " + token)
                .when()
                .get("posts/" + postId);
    }

    public Response getPostByTitle(String title, String token) {
        return given()
                .baseUri(BASE_URL)
                .header("Authorization", "Bearer " + token)
                .queryParams("title", title)
                .when()
                .get("posts/title/");
    }


    public Response updatePost(String postId, PostDTO postDTO, String token) {
        return given()
                .baseUri(BASE_URL)
                .body(postDTO)
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + token)
                .pathParams("postId", postId)
                .when()
                .put("posts/{postId}" );
    }

    public Response deletePost(String postId , String token) {
        return given()
                .baseUri(BASE_URL)
                .header("Authorization", "Bearer " + token)
                .pathParams("postId", postId)
                .when()
                .delete("posts/{postId}");
    }


}
