package com.isaac.dto;

public class Request {
    private long firstNumber;

    public long getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(long secondNumber) {
        this.secondNumber = secondNumber;
    }

    public Request(long secondNumber, long firstNumber) {
        this.secondNumber = secondNumber;
        this.firstNumber = firstNumber;
    }

    public long getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(long firstNumber) {
        this.firstNumber = firstNumber;
    }

    private long secondNumber;

}
