package com.isaac;

import com.isaac.dto.UserSignInDTO;
import com.isaac.dto.UserSignUpDTO;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class UserAPI {

    private static final String BASE_URL = "http://localhost:9091/api/v1/";

    public Response registerUser(UserSignUpDTO userSignUpDTO) {
        return given()
                .baseUri(BASE_URL)
                .body(userSignUpDTO)
                .contentType(ContentType.JSON)
                .when()
                .post("auth/signup");
    }

    public Response loginUser(UserSignInDTO userSignInDTO) {
        return given()
                .baseUri(BASE_URL)
                .body(userSignInDTO)
                .contentType(ContentType.JSON)
                .when()
                .post("auth/signin");
    }
}